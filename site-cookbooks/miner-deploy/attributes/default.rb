default['common']['deploy_user'] = 'miner'
default['common']['deploy_group'] = 'miner'

default['nginx']['dir'] = '/etc/nginx'
default['nginx']['port'] = 80
default['nginx']['application'] = 'library-miner'
